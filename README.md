以滑动的方式动态刷新Grid View或者Table View的内容。具体来说，对于Grid View（网格视图）的每个子视图的内容，新内容从上往下移动代替旧内容；对于Table View（表格）的每一行单元格（Cell），新内容从左往右移动代替旧内容。	
